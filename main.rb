require_relative "modules/bowling_game"
require_relative "modules/scorecard"
require_relative "modules/tournament"
require_relative 'fixtures/valid_data'
require_relative 'fixtures/invalid_data'

# ****************************************************
# Creates a tournament with 1000 games, default is 5
Tournament.new 1000
Tournament.report

# Some setup using data from fixtures/data
# refer to test cases in unit_tests
# default = Scorecard.new(ValidData::DEFAULT)
# all_strikes = Scorecard.new(ValidData::ALL_STRIKES)
# strikes_second_last = Scorecard.new(ValidData::STRIKES_SECOND_LAST)
# wrong_numer_of_frames = Scorecard.new(InvalidData::WRONG_FRAMES)
# wrong_entries_last_frame = Scorecard.new(InvalidData::WRONG_ENTRIES_LAST_FRAME)
# alternative_strikes = Scorecard.new(ValidData::ALT_STRIKES)
# five_strikes = Scorecard.new(ValidData::FIVE_STRIKES)
# invalid_scores = Scorecard.new(InvalidData::INVALID_SCORES_1)

# two_strikes = Scorecard.new(ValidData::TWO_STRIKES)
# game = BowlingGame.new(two_strikes)
# game.report

# all_strikes = Scorecard.new(ValidData::ALL_STRIKES)
# game2 = BowlingGame.new(all_strikes)
# game2.report

# game3 = BowlingGame.new(strikes_second_last)
# game3.report

# default game is 10 pin and 10 frames if no arguments passed to BowlingGame.new
# for the required output:-
# game = BowlingGame.new(default)
# processed = game.process
# p game.cumulative_scores

