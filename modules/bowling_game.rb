require_relative 'score_processor'

class BowlingGame
  include ScoreProcessor
  attr_accessor :pins, :frames, :scorecard

  def initialize(scorecard, pins=10,frames=10)
    @scorecard = scorecard
    @pins = pins
    @frames = frames
    # check validity of scorecard on initialisation. raises error if fails checks
    valid?
  end

  def report
    process
    puts %Q{
    Final Score: #{sum_all_frames}
    Score for each frame: #{sum_each_frame}
    Cumulative Scores for all frames: #{cumulative_scores}
    Adjusted Scorecard: #{scorecard.frames}
    }
  end

end