module ScoreProcessor
  # private
  def sum_all_frames
    scorecard.frames.flatten.reduce(:+)
  end

  def sum_each_frame
    scorecard.frames.map { |frame| frame.reduce(:+) }
  end
  
  def cumulative_scores
    sum = 0
    sum_each_frame.map { |frame| sum += frame }
  end
  
  def strike?(pins_down)
    pins_down == pins
  end

  # input validation checks
  def valid?
   scores_valid? && 
   number_of_frames_valid? && 
   frame_scores_valid? && 
   scores_count_valid?
  end
  # check that each throw cannot be more than :pins
  def scores_valid?
    unless scorecard.frames.flatten.any? { |score| score <= pins }
      raise "One or more of your scores are invalid. i.e. a score might be more than the number of pins."
    end
    true
  end
  # check if number of frames are correct
  def number_of_frames_valid?
    unless scorecard.frames.size == frames
      raise "Your scorecard is either incomplete or it has more frames than is allowed (#{frames} frames are allowed for a complete game)"
    end
    true
  end
  # check that total points for each frame cannot be more than :pins,
  # and that last frame cannot have total points more than :pins * 3
  def frame_scores_valid?
    unless scorecard.frames[0..-2].all? { |frame| frame.reduce(:+) <= pins } && 
      scorecard.frames[-1].reduce(:+) <= pins * 3
      raise "one or more of your frames have invalid total points."
    end
    true
  end
  # checks if scorecard has valid number of throws per frame. and that last frame cannot have more than 3 throws
  def scores_count_valid?
    unless scorecard.frames[0..-2].all? { |frame| points_count_check_1(frame) } && 
      points_count_check_2(scorecard.frames[-1])
      raise "one or more of your frames have invalid entries."
    end
    true
  end
  # check number of throws for first to second last frame
  def points_count_check_1(frame)
    if frame[0] == pins
      frame.size == 1
    else
      frame.size == 2
    end
  end
  # check number of throws for last frame
  def points_count_check_2(frame)
    if frame[0] == pins
      frame.size == 3
    else
      frame.size == 2
    end
  end

  # adjusts scorecard for strikes
  def process
    frames = scorecard.frames
    i = 0
    while i < frames.size - 1
      frame = frames[i]
      # if the first score of a frame is a strike
      if strike? frame[0]
        consecutive_strikes = 1
        j = i
        # check how many consecutive strikes are there for each frame
        # we only check up to 3 consecutive strikes and 2 consecutive strikes for the second last frame
        while consecutive_strikes < index_check(i) && strike?(frames[j+1][0])
          j += 1
          consecutive_strikes += 1
        end
        # bonus calculator returns the bonus points base on the consecutive strike count and concats that to current frame
        frame.concat bonus_calculator(consecutive_strikes, i)
      end
      i += 1
    end
    frames
  end

  def bonus_calculator(consecutive_strikes, index)
    frames = scorecard.frames
    case consecutive_strikes
    when 1
      return frames[index + 1][0..1]
    when 2
      return [pins, frames[index + 2][0]]
    when 3
      return [pins, pins]
    else
      puts "The rules do not allow for accounting for more than 3 consecutive strikes"
    end
  end
  # if index is at second last frame, we only want to check for up to 2 consecutive strikes
  def index_check(index)
    index < scorecard.frames.size - 2 ? 3 : 1
  end

end