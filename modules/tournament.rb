# require_relative 'score_processor'
require_relative 'bowling_game'
require_relative '../fixtures/valid_data'

class Tournament
  include ValidData
  attr_accessor :pins, :frames

  @@games = []
  def initialize(count=5, pins=10, frames=10)
    @pins = pins
    @frames = frames
    count.times { @@games << BowlingGame.new(generate_scorecards, pins, frames) }
  end

  def self.report
    @@games.each { |game| game.report }
  end
  
  def select_scorecards
    size = ValidData.constants(false).size
    data = ValidData.constants(false)
    Scorecard.new ValidData.const_get(data[rand(size)])
  end

  def generate_scorecards
    new_scorecard = []
    
    frames.times do
      first_throw = rand(pins+1)
      new_scorecard << ( first_throw == pins ? [] << first_throw : [] << first_throw << rand(pins - first_throw) )
    end
    # if the first throw of the last frame is a strike, then append two more throws. if strike again, then refresh pins
    if new_scorecard[-1][0] == pins
      first_throw = rand(pins+1)
      new_scorecard[-1] << first_throw << ( first_throw == pins ? rand(pins) : rand(pins - first_throw) )
    end

    Scorecard.new new_scorecard
  end

end
