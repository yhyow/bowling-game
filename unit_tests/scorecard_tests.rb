require 'test/unit'
require_relative '../modules/bowling_game'
require_relative '../modules/scorecard'
require_relative '../modules/tournament'
require_relative '../fixtures/valid_data'
require_relative '../fixtures/invalid_data'

class InvalidScorecardTest < Test::Unit::TestCase
  # last frame has a missing score because a strike requires 3 entries in the last frame
  def test_invalid_scores_1
    assert_raise(RuntimeError) { BowlingGame.new(Scorecard.new InvalidData::INVALID_SCORES_1) }
  end
  # a strike requires that a frame only has one entry unless it is the last frame
  def test_invalid_scores_2
    assert_raise(RuntimeError) { BowlingGame.new(Scorecard.new InvalidData::INVALID_SCORES_2) }
  end
  # last frame has 4 entries instead of the allowed 3 if last frame is a strike
  def test_invalid_scores_3
    assert_raise(RuntimeError) { BowlingGame.new(Scorecard.new InvalidData::INVALID_SCORES_3) }
  end
  # last frame has 2 entries instead of the allowed 3 if last frame is a strike
  def test_invalid_scores_4
    assert_raise(RuntimeError) { BowlingGame.new(Scorecard.new InvalidData::INVALID_SCORES_4) }
  end
  # a frame has total points of more than :pins
  def test_invalid_scores_5
    assert_raise(RuntimeError) { BowlingGame.new(Scorecard.new InvalidData::INVALID_SCORES_5) }
  end
  # there are more frames than allowed
  def test_invalid_scores_6
    assert_raise(RuntimeError) { BowlingGame.new(Scorecard.new InvalidData::INVALID_SCORES_6) }
  end

end

class ValidScorecardTest < Test::Unit::TestCase
  # test that default test case sums to 100 points
  def test_default_testcase
    default = Scorecard.new(ValidData::DEFAULT)
    game = BowlingGame.new(default)
    game.process
    assert_equal game.cumulative_scores, [8, 18, 27, 42, 47, 54, 57, 74, 81, 100]
  end
  
  def test_all_strikes
    all_strikes = Scorecard.new(ValidData::ALL_STRIKES)
    game = BowlingGame.new(all_strikes)
    game.process
    assert_equal game.cumulative_scores, [30, 60, 90, 120, 150, 180, 210, 240, 270, 300]
  end
  
  def test_five_strikes
    five_strikes = Scorecard.new(ValidData::FIVE_STRIKES)
    game = BowlingGame.new(five_strikes)
    game.process
    assert_equal game.cumulative_scores, [30, 60, 90, 120, 140, 150, 150, 150, 150, 150]
  end
  
  def test_alternative_strikes
    alternative_strikes = Scorecard.new(ValidData::ALT_STRIKES)
    game = BowlingGame.new(alternative_strikes)
    game.process
    assert_equal game.cumulative_scores, [10, 10, 20, 20, 30, 30, 40, 40, 50, 50]
  end
  
  def test_strikes_last_frame
    strikes_last_frame = Scorecard.new(ValidData::STRIKES_SECOND_LAST)
    game = BowlingGame.new(strikes_last_frame)
    game.process
    assert_equal game.cumulative_scores, [0, 0, 0, 0, 0, 0, 0, 0, 13, 16]
  end
  
  def test_strike_second_last_frame
    strikes_second_last_frame = Scorecard.new(ValidData::STRIKES_LAST)
    game = BowlingGame.new(strikes_second_last_frame)
    game.process
    assert_equal game.cumulative_scores, [0, 0, 0, 0, 0, 0, 0, 0, 0, 30]
  end

end


#
# game2 = BowlingGame.new(all_strikes)
# game2.report
#
# game3 = BowlingGame.new(alternative_strikes)
# game3.report
#
# game4 = BowlingGame.new(five_strikes)
# game4.report