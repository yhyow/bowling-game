module InvalidData
  # last frame has a missing score because a strike requires 3 entries in the last frame
  INVALID_SCORES_1 = [[6,2],[9,1],[1,8],[10],[4,1],[15,2],[0,3],[10],[7,0],[10,7,2]]
  # a strike requires that a frame only has one entry unless it is the last frame
  INVALID_SCORES_2 = [[6,2],[9,1],[1,8],[10,2],[4,1],[9,1],[0,3],[10],[7,0],[10,7,2]]
  # last frame has 4 entries instead of the allowed 3 if last frame is a strike
  INVALID_SCORES_3 = [[6,2],[9,1],[1,8],[10],[4,1],[9,1],[0,3],[10],[7,0],[10,7,2,10]]
  # last frame has 2 entries instead of the allowed 3 if last frame is a strike
  INVALID_SCORES_4 = [[6,2],[9,1],[1,8],[10],[4,1],[5,2],[0,3],[10],[7,0],[10,7]]

  INVALID_SCORES_5 = [[6,2],[9,2],[1,8],[10],[4,1],[5,2],[0,3],[10],[7,0],[10,7]]
  # there are more frames than allowed
  INVALID_SCORES_6 = [[10],[10],[10],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]]
end